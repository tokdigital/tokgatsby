import * as React from "react"
import "@fontsource/poppins"
import '../css/global.css'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faFacebook, faLinkedin, faYoutube } from "@fortawesome/free-brands-svg-icons"

import logo from "../images/logo.svg"
import menu from "../images/menu.svg"


// markup
const IndexPage = () => {
  return (
    <div className="container mx-auto">

      <header className="header py-10">
        <a href="/"><img className="logo float-left" src={logo} alt="Tok Digital Agency" /></a>
        <img className="menu-icon float-right cursor-pointer" src={menu} alt="Tok Menu" />
      </header>

      <FontAwesomeIcon icon={faFacebook} />

      <section className="section-hero w-full flex items-center text-white">
        <div className="w-full">
          <h1 className="font-bold text-7xl text-center py-20">We create unique digital<br /> solutions that<br /> tell a story</h1>
          <p class="mt-10 text-xl text-center">Since 2016, TOK Digital Agency has distinguished itself as a growth-oriented agency that<br /> understands its clients and their buyers. We provide a creative team focused on the<br /> communication and explanation of the most complex messages.</p>
        </div>
      </section>

    </div>
  )
}

export default IndexPage

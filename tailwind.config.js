module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      'body': ['Poppins'],
    },
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
